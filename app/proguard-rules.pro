# Preverification is irrelevant for the dex compiler and the Dalvik VM

-dontpreverify

# When not preverifing in a case-insensitive filing system

-dontusemixedcaseclassnames

# To repackage classes on a single package. Reduces size of output

-repackageclasses
-allowaccessmodification

# Specifies to write out extra information during processing

-verbose

# Use 3 steps of optimization

-optimizationpasses 3

# Switch off some optimizations that trip older versions of the Dalvik VM
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizations !code/allocation/variable
-optimizations !method/inlining/*

# Keep a fixed source file attribute and all line number tables to get line numbers in the stack traces
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

# Specifies not to ignore non-public library classes
-dontskipnonpubliclibraryclasses

# Maintain java native methods
-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.content.Context {
   public void *(android.view.View);
   public void *(android.view.MenuItem);
}

# Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}

# To keep annotations
-keepattributes *Annotation*

# Maintain enums
-keepclassmembers,allowoptimization enum * {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# To keep inner classes
-keepattributes InnerClasses
-keepattributes EnclosingMethod

# To remove debug logs:
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# RxJava
-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

# Okio FastNetworking
-dontwarn okio.**

# Okhttp
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**

# Workmanager
-keep class * extends androidx.work.Worker
-keep class * extends androidx.work.InputMerger
# Keep all constructors on ListenableWorker, Worker (also marked with @Keep)
-keep public class * extends androidx.work.ListenableWorker {
    public <init>(...);
}
# We need to keep WorkerParameters for the ListenableWorker constructor
-keep class androidx.work.WorkerParameters



