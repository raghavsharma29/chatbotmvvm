package com.raghav.chatbot.logic

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PagedList.Config
import com.raghav.chatbot.SingleLiveEvent
import com.raghav.chatbot.model.BotResponseModel
import com.raghav.chatbot.repository.ChatRepository
import com.raghav.chatbot.ui.MessageViewModel
import com.raghav.chatbot.ui.Sender
import com.raghav.chatbot.ui.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.util.*

class ChatViewModel(private val repository: ChatRepository) : ViewModel() {
    internal val showToast = SingleLiveEvent<String>()

    internal fun onSendClick(text: String, externalId: String, isNetworkAvailable: Boolean) {
        val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())
        if (text.isBlank()) {
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            repository.addToChat(text, currentDateTimeString, externalId, Sender.ME, Status.UNSENT)
            if (isNetworkAvailable)
                getBotResponse(text, externalId)
        }
    }

    internal fun getAllMessages(externalId: String): LiveData<PagedList<MessageViewModel>> {
        val config = Config.Builder().setEnablePlaceholders(false)
            .setPageSize(5)
            .build()
        return LivePagedListBuilder(repository.getAllMessages(externalId), config).build()
    }

    private fun getBotResponse(text: String, externalId: String) {
        repository.getBotResponse(text, externalId)
            .onSuccess { onBotResponseSuccess(it, text, externalId) }
            .onFailure { onBotResponseFailure(it) }
    }

    private fun onBotResponseFailure(throwable: Throwable) {
        showToast.postValue(throwable.message)
    }

    private fun onBotResponseSuccess(
        botResponseModel: BotResponseModel,
        text: String,
        externalId: String
    ) {
        botResponseModel.let {
            repository.updateChatStatus(text, externalId)
            val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())
            repository.addToChat(
                it.message.message,
                currentDateTimeString,
                externalId,
                Sender.BOT,
                Status.SENT
            )
        }
    }

    fun refreshData() {
        ioWork {
            val offlineMessages = repository.getOfflineMessages()
            offlineMessages.forEach {
                getBotResponse(it.message!!, it.externalChatId!!)
            }
        }
    }

    // High Order function for IO works
    private fun ioWork(block: () -> Unit): Job {
        return viewModelScope.launch(Dispatchers.IO) {
            block()
        }
    }
}
