package com.raghav.chatbot.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.me_message.*

class ChatAdapter : PagedListAdapter<MessageViewModel, ChatAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        private var DIFF_CALLBACK = object : ItemCallback<MessageViewModel>() {
            override fun areItemsTheSame(oldItem: MessageViewModel, newItem: MessageViewModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MessageViewModel, newItem: MessageViewModel): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = if (viewType == 0) {
            LayoutInflater.from(parent.context).inflate(com.docsapp.chatbot.R.layout.bot_message, parent, false)

        } else {
            LayoutInflater.from(parent.context).inflate(com.docsapp.chatbot.R.layout.me_message, parent, false)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = getItem(position) as MessageViewModel
        if (message.sender == Sender.ME) {
            if (message.status == Status.UNSENT) {
                holder.status.setImageResource(com.docsapp.chatbot.R.drawable.baseline_schedule_black_18)
            } else {
                holder.status.setImageResource(com.docsapp.chatbot.R.drawable.baseline_done_black_18)
            }
        }
        holder.text.text = message.text
        holder.time.text = message.time
    }

    override fun getItemViewType(position: Int): Int {
        super.getItemViewType(position)
        val message = getItem(position) as MessageViewModel
        return if (message.sender == Sender.BOT) {
            0
        } else 1
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View?
            get() = itemView
    }
}
