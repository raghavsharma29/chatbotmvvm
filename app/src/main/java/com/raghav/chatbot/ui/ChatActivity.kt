package com.raghav.chatbot.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.docsapp.chatbot.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar.*

class ChatActivity : AppCompatActivity() {
    private lateinit var fragment: ChatFragment
    private var drawerToggle: ActionBarDrawerToggle? = null
    private val DELAY_FOR_SMOOTH_NAV_DRAWER_CLOSE = 200
    private var chattingPerson: String = "chirag"
    private val isNavDrawerOpen: Boolean
        get() = if (drawerLayout != null) {
            drawerLayout!!.isDrawerOpen(GravityCompat.START)
        } else {
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setFragment(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setHomeButtonEnabled(true);
        initialiseNavDrawer()
    }

    private fun setFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            fragment = ChatFragment.getInstance(chattingPerson)
            supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commit()
        }
    }

    private fun initialiseNavDrawer() {
        drawerToggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.nav_open, R.string.nav_close
        )
        drawerLayout!!.addDrawerListener(drawerToggle!!)
        drawerToggle!!.syncState()

        navigationView!!.setNavigationItemSelectedListener { item ->
            closeDrawer()
            //Handler().postDelayed({ onNavigationItemClick(item) }, DELAY_FOR_SMOOTH_NAV_DRAWER_CLOSE.toLong())
            false
        }
    }

    /*private fun onNavigationItemClick(item: MenuItem) {
        item.isChecked = true
        when (item.itemId) {
            R.id.chat_chirag -> {
                if (!chattingPerson.equals("chirag", true)) {
                    chattingPerson = "chirag"
                    fragment.notifyExternalIdChanged(chattingPerson)
                }
            }
            R.id.chat_raghav -> {
                if (!chattingPerson.equals("raghav", true)) {
                    chattingPerson = "raghav"
                    fragment.notifyExternalIdChanged(chattingPerson)
                }
            }
        }
    }*/

    private fun closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                openDrawer()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (isNavDrawerOpen) {
            closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    private fun openDrawer() {
        if (drawerLayout != null) {
            drawerLayout!!.openDrawer(GravityCompat.START)
        }
    }
}