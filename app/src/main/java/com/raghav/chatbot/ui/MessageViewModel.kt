package com.raghav.chatbot.ui

data class MessageViewModel(
        var id: String? = null,
        var text: String? = null,
        var time: String? = null,
        var sender: Sender? = null,
        var status: Status? = null
)

enum class Status {
    SENT, UNSENT
}

enum class Sender {
    BOT, ME
}
