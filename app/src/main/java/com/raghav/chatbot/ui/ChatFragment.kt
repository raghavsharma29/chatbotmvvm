package com.raghav.chatbot.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.docsapp.chatbot.R
import com.raghav.chatbot.logic.ChatViewModel
import com.raghav.chatbot.repository.InjectorUtils
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : Fragment() {
    private val viewModel: ChatViewModel by viewModels {
        InjectorUtils.provideGardenPlantingListViewModelFactory(context!!)
    }

    private val networkChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (isNetworkAvailable()) {
                viewModel.refreshData()
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            return true
        }
        return false
    }

    /*override fun refreshListWithUpdatedData() {
        recyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun showMessage(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun getExternalID(): String {
        return arguments?.getString("externalId")!!
    }
*/
    private fun initRecyclerView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = ChatAdapter()
        (recyclerView.adapter as ChatAdapter).registerAdapterDataObserver(object :
            RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                recyclerView.smoothScrollToPosition(positionStart + itemCount)
            }
        })
    }

    companion object {
        fun getInstance(externalId: String): ChatFragment {
            val fragment = ChatFragment()
            val bundle = Bundle().apply { putString("externalId", externalId) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        initRecyclerView()
        subscribeUi()
    }

    private fun subscribeUi() {
        viewModel.getAllMessages(arguments!!.getString("externalId")!!)
            .observe(viewLifecycleOwner, Observer { result ->
                if (!result.isNullOrEmpty()) {
                    (recyclerView.adapter as ChatAdapter).submitList(result)
                }
            })

        viewModel.showToast.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })
    }

    override fun onStart() {
        super.onStart()
        val inputFilter = IntentFilter()
        inputFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        context!!.registerReceiver(networkChangeReceiver, inputFilter)
    }

    override fun onStop() {
        super.onStop()
        context!!.unregisterReceiver(networkChangeReceiver)
    }

    private fun setListener() {
        sendMessage.setOnClickListener {
            arguments?.getString("externalId")?.let { it1 ->
                viewModel.onSendClick(input.text.toString().trim(), it1, isNetworkAvailable())
            }
            input.text.clear()
        }
    }

    /*fun notifyExternalIdChanged(externalId: String) {
        presenter.setExternalId(externalId)
    }*/
}


