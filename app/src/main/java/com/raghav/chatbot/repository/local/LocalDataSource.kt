package com.raghav.chatbot.repository.local

import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import com.raghav.chatbot.db.Chat
import com.raghav.chatbot.db.ChatDAO

class LocalDataSource(private val dao: ChatDAO) {
    internal fun getOfflineMessages(): List<Chat> {
        return dao.getOfflineMessage()
    }

    @WorkerThread
    internal fun updateStatus(externalId: String, text: String?): Int {
        return dao.update(externalId, text)
    }

    @WorkerThread
    internal fun insertMessage(chat: Chat): Long {
        return dao.insert(chat)
    }

    @WorkerThread
    internal fun getAllChatMessages(externalID: String): DataSource.Factory<Int, Chat> {
        return dao.getAllMessages(externalID)
    }
}
