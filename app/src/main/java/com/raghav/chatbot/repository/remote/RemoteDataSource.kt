package com.raghav.chatbot.repository.remote

import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.ANResponse
import com.androidnetworking.common.Priority
import com.raghav.chatbot.model.BotResponseModel

class RemoteDataSource {
    fun getResponse(message: String, externalId: String): Result<BotResponseModel> {
        val request = AndroidNetworking.get("https://www.personalityforge.com/api/chat/")
                .addQueryParameter("apiKey", "6nt5d1nJHkqbkphe")
                .addQueryParameter("chatBotID", "63906")
                .addQueryParameter("externalID", externalId)
                .addQueryParameter("message", message)
                .setPriority(Priority.IMMEDIATE)
                .build()

        val response = request.executeForObject(BotResponseModel::class.java) as? ANResponse<BotResponseModel>
        return if (response != null && response.isSuccess)
            Result.success(response.result)
        else {
            Result.failure(response!!.error.fillInStackTrace())
        }
    }
}
