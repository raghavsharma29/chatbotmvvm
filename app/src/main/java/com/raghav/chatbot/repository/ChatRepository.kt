package com.raghav.chatbot.repository

import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import com.raghav.chatbot.db.Chat
import com.raghav.chatbot.db.ChatDAO
import com.raghav.chatbot.model.BotResponseModel
import com.raghav.chatbot.repository.local.LocalDataSource
import com.raghav.chatbot.repository.remote.RemoteDataSource
import com.raghav.chatbot.ui.MessageViewModel
import com.raghav.chatbot.ui.Sender
import com.raghav.chatbot.ui.Status

class ChatRepository private constructor(chatDao: ChatDAO) {
    private val localDataSource: LocalDataSource = LocalDataSource(chatDao)
    private val remoteDataSource: RemoteDataSource = RemoteDataSource()

    @WorkerThread
    fun addToChat(
        text: String,
        currentDateTimeString: String?,
        externalId: String,
        sender: Sender,
        status: Status
    ) {
        val chat = Chat()
        chat.externalChatId = externalId
        chat.message = text
        chat.sender = sender.ordinal
        chat.status = status.ordinal
        chat.time = currentDateTimeString
        localDataSource.insertMessage(chat)
    }

    @WorkerThread
    fun getBotResponse(text: String, externalId: String): Result<BotResponseModel> {
        return remoteDataSource.getResponse(text, externalId)
    }

    @WorkerThread
    fun updateChatStatus(text: String, externalId: String) {
        localDataSource.updateStatus(externalId, text)
    }

    @WorkerThread
    fun getAllMessages(externalId: String): DataSource.Factory<Int, MessageViewModel> {
        return localDataSource.getAllChatMessages(externalId).map<MessageViewModel> {
            val messageViewModel = MessageViewModel()
            messageViewModel.id = it.messageId.toString()

            if (it.sender == 0) {
                messageViewModel.sender = Sender.BOT
            } else {
                messageViewModel.sender = Sender.ME
            }

            if (it.status == 0) {
                messageViewModel.status = Status.SENT
            } else {
                messageViewModel.status = Status.UNSENT
            }

            messageViewModel.text = it.message
            messageViewModel.time = it.time
            messageViewModel
        }
    }


    @WorkerThread
    fun getOfflineMessages(): List<Chat> {
        return localDataSource.getOfflineMessages()
    }

    companion object {
        @Volatile
        private var instance: ChatRepository? = null

        fun getInstance(chatDao: ChatDAO): ChatRepository {
            return instance ?: synchronized(this) {
                instance
                    ?: ChatRepository(chatDao).also { instance = it }
            }
        }
    }
}
