package com.raghav.chatbot.repository

import android.content.Context
import com.raghav.chatbot.ui.ChatViewModelFactory
import com.raghav.chatbot.repository.local.AppDatabase

class InjectorUtils {
    companion object {
        fun provideGardenPlantingListViewModelFactory(context: Context): ChatViewModelFactory {
            val repository = getChatRepository(context)
            return ChatViewModelFactory(repository)
        }

        private fun getChatRepository(context: Context): ChatRepository {
            return ChatRepository.getInstance(AppDatabase.getInstance(context.applicationContext).getChatDao())
        }
    }
}
