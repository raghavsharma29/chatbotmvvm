package com.raghav.chatbot.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chat_table")
data class Chat(
    @PrimaryKey(autoGenerate = true)
    var messageId: Int? = null,
    var message: String? = null,
    var sender: Int? = null,
    var status: Int? = null,
    var externalChatId: String? = null,
    var time: String? = null
)
