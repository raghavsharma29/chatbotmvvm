package com.raghav.chatbot.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ChatDAO {
    @Query("SELECT * from chat_table where externalChatId = :externalChatId")
    fun getAllMessages(externalChatId: String): DataSource.Factory<Int, Chat>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chat: Chat): Long

    @Query("UPDATE chat_table SET status = 0 where message LIKE :text and externalChatId = :externalChatId")
    fun update(externalChatId: String, text: String?): Int

    @Query("SELECT * from chat_table where status = 1")
    fun getOfflineMessage(): List<Chat>

    @Query("SELECT * from chat_table where status = 1")
    fun getOfflineMessageObservable(): LiveData<List<Chat>>
}