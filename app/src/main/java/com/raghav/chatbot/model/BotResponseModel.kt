package com.raghav.chatbot.model

import com.google.gson.annotations.SerializedName

data class BotResponseModel(
        @SerializedName("data")
    val `data`: List<Any>,
        @SerializedName("errorMessage")
    val errorMessage: String,
        @SerializedName("message")
    val message: Message,
        @SerializedName("success")
    val success: Int
) {
    data class Message(
        @SerializedName("chatBotID")
        val chatBotID: Int,
        @SerializedName("chatBotName")
        val chatBotName: String,
        @SerializedName("emotion")
        val emotion: String,
        @SerializedName("message")
        val message: String
    )
}